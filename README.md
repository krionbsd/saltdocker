# Supported tags and respective `Dockerfile` links

- [`3005rc1`, `rc`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
- [`3004.2`, `3004.1`, `3004`, `latest`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
- [`3004rc1`, `rc`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
- [`3003.5`, `3003.4`, `3003.3`, `3003.2`, `3003.1`, `3003`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
- [`3002.9`, `3002.8`, `3002.7`, `3002.6`, `3002.5`, `3002.4`, `3002.3`, `3002.2`, `3002.1`, `3002`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.Dockerfile)
- [`3001.8`, `3001.7`, `3001.6`, `3001.5`, `3001.4`, `3001.3`, `3001.2`, `3001.1`, `3001`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.old.Dockerfile)
- [`3000.9`, `3000.8`, `3000.7`, `3000.6`, `3000.5`, `3000.4`, `3000.3`, `3000.2`, `3000.1`, `3000`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.old.Dockerfile)
- [`2019.2.0`, `2019.2`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.old.Dockerfile)
- [`2018.3.4`, `2018.3`](https://gitlab.com/saltstack/open/saltdocker/-/blob/master/cicd/docker/alpine.old.Dockerfile)

# What is Salt?

Salt is a configuration management tool / orchestration platform.

This image contains a running salt-master and salt-api process, which can be used to control other salt-minions.

![SaltProject](https://gitlab.com/saltstack/open/salt-branding-guide/-/raw/master/logos/SaltProject_altlogo_teal.png)

# How to use this image

## Start a Salt instance

```console
$ docker run --name salt --hostname salt -p 4505-4506:4505-4506 -p 8000:8000 -e SALT_SHARED_SECRET=mysecretpassword -d saltstack/salt
```

The default `salt` user is created but the shared secret is specified in the `/etc/salt/master.d/api.conf`.

The api listens on port `8000` with `ssl` enabled.

# How to extend this image

There are many ways to extend the `salt` image. Without trying to support every possible use case, here are just a few that we have found useful.

## Environment Variables

The Salt image uses several environment variables which are easy to miss. While none of the variables are required, they may significantly aid you in using the image.

### `SALT_MASTER_CONFIG`

A JSON object. This variable is dumped to /etc/salt/master.d/master.conf and can be used to provide extra config for the salt master.

### `SALT_API_CONFIG`

A JSON object. This variable is dumped to /etc/salt/master.d/api.conf, and defaults to the following.

```yaml
rest_cherrypy:
  port: 8000,
  ssl_crt: /etc/pki/tls/certs/localhost.crt
  ssl_key: /etc/pki/tls/certs/localhost.key
external_auth:
    sharedsecret:
        salt: ['.*', '@wheel', '@jobs', '@runner']
sharedsecret: $SALT_SHARED_SECRET
```

### `SALT_SHARED_SECRET`

If this environment variable is set, it will set the sharedsecret variable for using the salt-api with the salt user.

## Salt Wheel Modules

If the salt-master is not configured immediately at the start, the master config can be updated using wheel modules via the salt api using the [Salt Config Wheel Module](https://docs.saltproject.io/en/latest/ref/wheel/all/salt.wheel.config.html)

## Volumes for Salt Keys

In order to make volumes available to the `salt` user in the container, assign the group id `450` to the directory before it mounting it on the container.
